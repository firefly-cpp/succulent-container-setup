<p align="center">
  <img alt="logo" width="300" src=".logos/images/logo.png">
</p>

<h1 align="center">
  succulent
</h1>

<p align="center">
  <a href="#-docker">🐳 Docker</a> •
  <a href="#-usage">🚀 Usage</a> •
  <a href="#-configuration">🔧 Configuration</a> •
  <a href="#-worker-threads">🧵 Worker threads</a>
</p>

[succulent](https://github.com/firefly-cpp/succulent) is a pure Python framework that simplifies the configuration, management, collection, and preprocessing of data collected via POST requests.

## 🐳 Docker
To begin using the container, you can either pull the image from the Docker Hub or build the image locally.

### Local environment
To build the container locally, clone the repository and navigate to the `succulent-container-setup` directory.

```bash
git clone https://codeberg.org/firefly-cpp/succulent-container-setup
cd succulent-container-setup
```

Next, build the Docker image using the following command:

```bash
docker build -t succulent-container-setup .
```

Finally, run the Docker container using the following command:

```bash
docker run -p 8080:8080 succulent-container-setup
```

### DockerHub
**Note**: Since the DockerHub image is already built, you will need to modify the configuration file to adapt it to your needs.

To pull the image from the DockerHub, execute the following command:

```bash
docker pull codeberg.org/firefly-cpp/succulent-container-setup/succulent-container:latest
```

Next, run the Docker container using the following command:

```bash
docker run -p 8080:8080 -v /path/on/disk:/succulent-app/configuration.yml codeberg.org/firefly-cpp/succulent-container-setup/succulent-container:latest
```

The `-v` flag is used to mount the configuration file to the container. The configuration file should be located in the specified path on the host machine.

## 🚀 Usage
Once the Docker container is running, `succulent` will store the data sent via POST requests.

### Data collection
To send data to the container, simply use the POST endpoint `/measure`. The data can be sent in either JSON format or in the parameters of the POST request.

#### JSON
The following cURL is an example of sending data in JSON format:

```cURL
curl --location 'http://127.0.0.1:8080/measure' \
--header 'Content-Type: application/json' \
--data '{
    "temperature": 15,
    "humidity": 5,
    "light": 100,
    "time": "11:59",
    "date": "2023-05-17"
}'
```

#### Parameters
The following cURL is an example of sending data in the parameters of the POST request:

```cURL
curl --location --request POST 'http://127.0.0.1:8080/measure?temperature=15&humidity=5&light=100&time=11%3A59&date=2023-05-17'
```

### Data access
The data can be accessed via the `succulent` API and via Docker commands.

#### API
To access the data via the `succulent` API, send a GET request to the `/data` endpoint after enabling the results option in the configuration file (see [configuration](#-configuration) for more information).

The data can also be directly downloaded in the format specified in the configuration file by sending a GET request to the `/export` endpoint.

#### Docker
To access the data stored in the Docker container, first acces the container shell using the following command:

```bash
docker exec -it <container_id> /bin/sh
```

Inside the container shell, navigate to the `/data` directory and access the data using the following commands:

```bash
cd /data
cat data.[format]
```

Alternatively, the data can be exported with Docker volumes. Use the following command to export the data:

```bash
docker run -p 8080:8080 -v /path/on/disk:/succulent-app/data succulent-container-setup
```

## 🔧 Configuration
### Data collection
In the root directory, create a `configuration.yml` file and define the following:
```yml
data:
  - name: # Measure name
    min:  # Minimum value (optional)
    max:  # Maximum value (optional)
```

To collect images, create a ``configuration.yml`` file in the root directory and define the following:
```yml
data:
  - key: # Key in POST request
```

To store data collection timestamps, define the following setting in the `configuration.yml` file in the root directory:
```yml
timestamp: true # false by default
```

To access the URL for data collection, send a GET request (or navigate) to [http://localhost:8080/measure](http://localhost:8080/measure).

To restrict access to the collected data, define the following setting in the `configuration.yml` file in the root directory:
```yml
password: 'password' # Password for data access
```

To store data using a password, append the password parameter to the request URL: `?password=password`.

### Data access
To access data via the Succulent API, define the following setting in the `configuration.yml` file in the root directory:
```yml
results:
  - enable: true # false by default
```

To access the collected data, send a GET request (or navigate) to [http://localhost:8080/data](http://localhost:8080/data). To access password-protected data, append the password parameter to the request URL: `?password=password`.

### Data export
To export the data, enable the export option in the configuration file:
```yml
results:
  - export: true # false by default
```

To export the data, send a GET request (or navigate) to [http://localhost:8080/export](http://localhost:8080/export). To export password-protected data, append the password parameter to the request URL: `?password=password`. The data will be downloaded in the format specified in the configuration file.

## 🧵 Worker threads
Depending on the number of threads the container has access to you can also set up `GUNICORN_WORKERS` environment variable. The suggested value is `2` to `4` per processor core.
